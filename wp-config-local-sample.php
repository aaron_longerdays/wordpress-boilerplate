<?php 
// Local server settings
// Change wp-config-local-sample.php to wp-config-local.php in order to use.

// Local Database
define('DB_NAME', 'local_db');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
 
// Overwrites the database to save keep edeting the DB
define('WP_HOME','http://localhost:8888/PATH_TO_SITE');
define('WP_SITEURL','http://localhost:8888/PATH_TO_SITE');
 
// Turn on debug for local environment
define('WP_DEBUG', true);